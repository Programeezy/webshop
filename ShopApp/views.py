from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import ListView

from .models import Game

ITEMS_ON_PAGE = 5


class GameList(ListView):
    model = Game
    template_name = 'Shop/product_list.html'
    paginate_by = ITEMS_ON_PAGE

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category_list'] = [cat[1] for cat in Game.CATEGORY_CHOICES]
        return context


def index(request):
    game_list = Game.objects.all()
    category_list = [cat[1] for cat in Game.CATEGORY_CHOICES]
    context = {
        'game_list': game_list[:ITEMS_ON_PAGE],
        'category_list': category_list,
    }

    return render(request=request, template_name='Shop/index.html', context=context)
