from django.db import models


class Game(models.Model):
    ACTION = 'Action'
    RPG = 'RPG'
    SURVIVAL = 'Survival'

    CATEGORY_CHOICES = (
        (ACTION, 'Action'),
        (RPG, 'RPG'),
        (SURVIVAL, 'Survival')
    )

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    category = models.CharField(max_length=50, default=ACTION, choices=CATEGORY_CHOICES)
    price = models.FloatField()
    price_on_sale = models.FloatField(null=True, blank=True)
    complete_price = models.FloatField(default=350.0)
    purchase_count = models.IntegerField(default=0)
    img = models.ImageField(max_length=100)
    game_code = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class KeyStorage(models.Model):
    id = models.AutoField(primary_key=True)
    game_code = models.CharField(max_length=100)
    value = models.CharField(max_length=20)
