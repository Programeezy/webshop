from django.conf.urls import url

from ShopApp import views

urlpatterns = [
    url('^$', view=views.GameList.as_view(), name='index'),
]
